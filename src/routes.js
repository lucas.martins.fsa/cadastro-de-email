import React from "react";

import {BrowserRouter as Router ,Switch ,Route, Link } from 'react-router-dom';

import Sign from './static/sign'

function Rotas() {
    return (        
        <Router>
            <div className='App'>                
                <Link to='/'></Link>                
            </div>
            <Switch>                
                <Route exact path="/" component={Sign}/>
                
            </Switch>

        </Router>
    )
}

export default Rotas;