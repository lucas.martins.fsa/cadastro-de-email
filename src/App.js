import React from 'react';
import Sign from './static/sign';

function Application() {  
  return (
    <>
      <div className='container'>
        <Sign/>
      </div>
    </>
  );
}

export default Application;
